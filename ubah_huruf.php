<?php
function ubah_huruf($string)
{
    $lower = strtolower($string);
    $arrStr = str_split($lower);
    $range = range('a', 'z');
    $newArr = [];
    for ($i = 0; $i <= count($arrStr) - 1; $i++) {
        for ($j = 0; $j <= count($range) - 1; $j++) {
            if ($range[$j] == $arrStr[$i]) {
                if ($arrStr[$i] == $range[count($range) - 1]) {
                    array_push($newArr, $range[0]);
                } else {
                    array_push($newArr, $range[$j + 1]);
                }
            }
        }
    }
    $newStr = implode("", $newArr);
    echo "String : " . $string . "<br>";
    echo $newStr . "<br><br>";
}

echo "2. Ubah Huruf <br>";
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
