<?php
function tukar_besar_kecil($string)
{
    $arrStr = str_split($string);
    $newArr = [];
    for ($i = 0; $i <= count($arrStr) - 1; $i++) {
        if ($arrStr[$i] == strtoupper($arrStr[$i])) {
            array_push($newArr, strtolower($arrStr[$i]));
        } else if ($arrStr[$i] == strtolower($arrStr[$i])) {
            array_push($newArr, strtoupper($arrStr[$i]));
        }
    }
    $newStr = implode("", $newArr);
    echo "String : " . $string . "<br>";
    echo $newStr . "<br><br>";
}

echo "3. Tukar Besar Kecil<br>";
// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
